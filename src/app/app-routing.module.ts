import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {AppMainComponent} from './app.main.component';
import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppErrorComponent} from './pages/app.error.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';
import { InicioComponent } from './sistema/inicio/inicio.component';
import { PessoaComponent } from './sistema/pessoa/pessoa.component';
import { PessoaListComponent } from './sistema/pessoa-list/pessoa-list.component';
import { AuthgardService } from './authgard.service';


@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: AppMainComponent,
                children: [
                    {path: '', component: InicioComponent, canActivate: [AuthgardService]},
                    {path: 'inicio', component: InicioComponent, canActivate: [AuthgardService]},
                    {path: 'pessoa-list', component: PessoaListComponent, canActivate: [AuthgardService]},
                    {path: 'pessoa/:id', component: PessoaComponent, canActivate: [AuthgardService]},
                ]
            },
            {path: 'error', component: AppErrorComponent},
            {path: 'access', component: AppAccessdeniedComponent},
            {path: 'notfound', component: AppNotfoundComponent},
            {path: 'login', component: AppLoginComponent},
            {path: '**', redirectTo: '/notfound'},
        ], {scrollPositionRestoration: 'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

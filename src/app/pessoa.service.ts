import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pessoa } from './sistema/pessoa/pessoa.component';


@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  constructor(private httpClient: HttpClient) { }

  public save(pessoa: Pessoa): Observable<any> {
    return this.httpClient.post("http://localhost:8080/pessoa", pessoa);
  }

  public getById(id){
    return this.httpClient.get(`http://localhost:8080/pessoa/${id}`);
  }

  public get(){
    return this.httpClient.get("http://localhost:8080/pessoa");
  }

}

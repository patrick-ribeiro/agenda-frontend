import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CepService {

  constructor(private http: HttpClient) { }

  public get(cep): Observable<any>{
    return this.http.get(`http://localhost:8080/cep/${cep}`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {

  constructor(private http: HttpClient) { }

  public get(nome): Observable<any> {
    return this.http.get(`http://localhost:8080/municipio/${nome}`);
  }

}

import {Component} from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
        <div class="footer">
            <div class="card clearfix">
                <span class="footer-text-left">Agenda WEB versão 2.00</span>
                <span class="footer-text-right">
                    <span>Todos os direitos reservados</span>
                </span>
            </div>
        </div>
    `
})
export class AppFooterComponent {

}

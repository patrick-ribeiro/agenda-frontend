import { Component } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
})
export class AppLoginComponent {

  public usuario = "";
  public senha = "";

  constructor(private loginService: LoginService, private router: Router){}

  logar(){
    this.loginService.logar(this.usuario, this.senha).subscribe(r => {
      localStorage.setItem("ads04_access_token", r.access_token);
      this.router.navigate(['/']);
    });
  }

}

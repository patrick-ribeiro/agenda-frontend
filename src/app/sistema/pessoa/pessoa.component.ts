import { Component, OnInit } from '@angular/core';
import { PessoaService } from 'src/app/pessoa.service';
import { MunicipioService } from 'src/app/municipio.service';
import { ActivatedRoute } from '@angular/router';
import { CepService } from 'src/app/cep.service';

export class Pessoa{

  constructor(){}

	id: Number;
	nome: String;
	telefone: String;
  nascimento: Date;
  cep: String;
  endereco: String;
  bairro: String;
  municipio: String;
}

@Component({
  selector: 'app-pessoa',
  templateUrl: './pessoa.component.html',
  styleUrls: ['./pessoa.component.scss']
})
export class PessoaComponent implements OnInit {

  pessoa;

  municipios: string[] = ['a', 'aab', 'aaab'];


  constructor(
    private pessoaService: PessoaService, 
    private municipioService: MunicipioService, 
    private cepService: CepService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe( params => {
      let id = params['id'];
      if(id == 'nova'){
        this.pessoa = new Pessoa();
      }else{
        this.pessoaService.getById(id).subscribe(r => {
          this.pessoa = r;
        });
      }
    });
  }

  btnSalvar(){
    this.pessoaService.save(this.pessoa).subscribe(r =>{
      this.pessoa = new Pessoa();
    });
  }  

  consultarCep(){
    console.log("consultar cep");
    this.cepService.get(this.pessoa.cep).subscribe(r => {
      this.pessoa.bairro = r.bairro;
      this.pessoa.endereco = r.logradouro;
      this.pessoa.municipio = `${r.municipio.nome}/${r.municipio.uf}`;
    });
  }

  listarMunicipios(event) {
    let resp = [];
    this.municipioService.get(event.query).subscribe(r => {
      for(let m of r){
        resp.push(`${m.nome} - ${m.uf}`)
      }
      this.municipios = resp;
    });
  }  

}
